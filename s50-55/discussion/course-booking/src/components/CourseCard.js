import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import UserContext from '../UserContext.js';
// import the useState hook from react
import {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';
export default function CourseCard(props){
	// consume the content of the UserContext
	const {user} = useContext(UserContext);
	
	/*console.log(props.courseProp);*/

	// object destructuring
	const{_id, name, description, price} = props.courseProp;
	
	// Use the state hook for this component to be able to store its state
	// States are use to keep track of information related to individual components
	/*
		Syntax: const [getter, setter] = useState(
		initialGetterValue);
	*/
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	const [isDisabled, setIsDisabled] = useState(false);
	function enroll(){
		//console.log(count);
		//console.log(setCount);
		if(seat !== 0){
			setCount(count + 1);
			setSeat(seat - 1);
		}else{
			setIsDisabled(true);
			alert("No more seats available");
		}	
	}

	//console.log(isDisabled);
	//console.log(setIsDisabled);
	// the function or the side effect in our useEffect hook will be invoke or run on the initial loading of our application and when there is/are changes on our dependencies
	useEffect(()=>{
		if(seat === 0){
			setIsDisabled(true);
		}
	},[seat]);
	
	return(
		<Container className = "mt-5">
		<Row>
			<Col className = "col-12">
			<Card className = "cardHighlight courseCard">
			     <Card.Body>
			       <Card.Title>{name}</Card.Title>
			       <Card.Subtitle>Description:</Card.Subtitle>
			       <Card.Text>{description}</Card.Text>
			       <Card.Subtitle>Price:</Card.Subtitle>
			       <Card.Text>{price}</Card.Text>
			       <Card.Subtitle>Enrollees:</Card.Subtitle>
			       <Card.Text>{count} Enrollees</Card.Text>

			       {
			       	user !== null 
			       	?
			       	<Button as = {Link} to = {`/courses/${_id}`}>Details</Button> 
			       	:
			       	<Button as = {Link} to = '/login'>Login to enroll</Button>

			       }
			     </Card.Body>
			   </Card>
			</Col>
		</Row>
		</Container>

		)
}