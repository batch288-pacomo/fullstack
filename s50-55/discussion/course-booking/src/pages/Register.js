
import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext.js';

import {Link, useNavigate, Navigate} from 'react-router-dom';

export default function Register(){
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	//we consume the setUser function from the UserContext
	const {user, setUser} = useContext(UserContext);


	//we containe the useNavigate to navigate variable;
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	//we have to use the useEffect in enabling the submit button
	useEffect(()=>{
		if (email !== '' && password1 !=='' && password2 !== '' && password1 === password2 && password1.length > 6 && mobileNo.length >= 11){

			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}


	}, [email, password1, password2, mobileNo]);

	//function that will be triggered once we submit the form
	function register(event){
		//it prevents our pages to reload when submitting the forms
		event.preventDefault()
console.log(firstName);
console.log(lastName);
console.log(email);
console.log(password1);
console.log(mobileNo);
		fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password:password1,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				console.log(data);
				Swal2.fire({
				    title: 'Authentication failed!',
				    icon: 'error',
				    text: 'Check your register details and try again!'
				})
			}else{
				localStorage.setItem('firstName', firstName);
				setUser(localStorage.getItem('firstName'));
				localStorage.setItem('lastName', lastName);
				setUser(localStorage.getItem('lastName'));
				localStorage.setItem('email', email);
				setUser(localStorage.getItem('email'));
				localStorage.setItem('password', password1);
				setUser(localStorage.getItem('password'));
				localStorage.setItem('mobileNo', mobileNo);
				setUser(localStorage.getItem('mobileNo'));

				Swal2.fire({
				    title : 'Login Successful',
				    icon : 'success',
				    text: 'Welcome to Zuitt!'

				})
				navigate('/login')
			}
		})


		/*localStorage.setItem('email', email);
		setUser(localStorage.getItem('email'));

		alert('Thank you for registering!');
		//clear input fields
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
		navigate('/')*/
	}

return(
	user.id === null 
	?
	<Container className = 'mt-5'>
		<Row>
			<Col className = 'col-6 mx-auto'>
				<h1 className = 'text-center'>Register</h1>
				<Form onSubmit = {event => register(event)}>

				      <Form.Group className="mb-3" controlId="first-name">
				        <Form.Label>First Name</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	value = {firstName}
				        	onChange = {event => setFirstName(event.target.value)}
				        	placeholder="Enter you first name" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="last-name">
				        <Form.Label>Last Name</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	value = {lastName}
				        	onChange = {event => setLastName(event.target.value)}
				        	placeholder="Enter you last name" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	value = {email}
				        	onChange = {event => {
				        		console.log(event)
				        		setEmail(event.target.value)}}
				        	placeholder="Enter email" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="mobile-number">
				        <Form.Label>Mobile no.</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	value = {mobileNo}
				        	onChange = {event => setMobileNo(event.target.value)}
				        	placeholder="Enter you mobile number" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				        	placeholder="Password" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	placeholder="Retype your nominated password" />
				      </Form.Group>

				      <p>Have an account already? <Link to = '/login'>Log in here.</Link></p>


				      <Button variant="primary" type="submit"
				      		disabled = {isDisabled}>
				        Register
				      </Button>
				    </Form>

			</Col>
		</Row>
	</Container>
	
	:

	<Navigate to = '/login'/>

	)
}
