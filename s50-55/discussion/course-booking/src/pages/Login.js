import {useState, useEffect, useContext} from 'react';

import {Container, Row, Col, Alert, Form, Button } from 'react-bootstrap'
import {Navigate, Link, useNavigate} from 'react-router-dom';

//import sweet alert
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext.js';

export default function Login(){
    const [isDisabled, setIsDisabled] = useState(true);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const navigate = useNavigate();
    //we are going to consume or use the UserContext
    const {user, setUser} = useContext(UserContext);

    //get item method gets the value of the specified key from our local storage
    // const [user, setUser] = useState(localStorage.getItem('email'));
   
    
    const login = (e) => {
        e.preventDefault() //The preventDefault() method will prevent the link above from following the URL.
        
        // console.log(user);
        // alert('Login successful!');

        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(result => result.json())
        .then(data => {
            if(data === false){
                Swal2.fire({
                    title: 'Authentication failed!',
                    icon: 'error',
                    text: 'Check your login details and try again!'
                })
            }else{
                
                localStorage.setItem('token', data.auth);

                retrieveUserDetails(data.auth);

                Swal2.fire({
                    title : 'Login Successful',
                    icon : 'success',
                    text: 'Welcome to Zuitt!'
                })



                navigate('/')
            }
        })

        //Set the email of the authenticated user in the local storage
        //Syntax:
            //localStorage.setItem('propertyName', value)

       /* localStorage.setItem('email', email);
        setUser(localStorage.getItem('email'));

        setEmail('');
        setPassword('');
        setIsDisabled(true);

        navigate('/')*/

    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(data => {
            console.log(data);
            setUser({
                id : data._id,
                isAdmin: data.isAdmin
            });

        })
    }
    
    useEffect(() => {
        if(email && password){   
            setIsDisabled(false) 
        } else {
            setIsDisabled(true)
        }
    }
    ,[email, password])

  return (

  
  /*  (user.id === null) ?
*/    <Container className='mt-5'>
        <Row>
            <Col className='col-6 mx-auto'>

                <h1 className='text-center'>Login</h1>
                <Form onSubmit={login}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={e => setEmail(e.target.value)} 
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value = {password}
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Form.Group>
                     
                    <p>No account yet? <Link to = '/register'>Sign up here</Link></p>

                    <Button 
                        className='mt-3' 
                        variant="primary" 
                        type="submit" 
                        disabled={isDisabled}
                    >
                        Login
                    </Button>
                </Form>
            </Col>
        </Row>
    </Container> 

    // :

    // <Navigate to = '/notAccessible'/>
  
    
  )
}
