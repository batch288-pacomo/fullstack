import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


// createRoot() - assigns the element to be managed by React with its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));
// rendec() - displaying the compenent/react element in to the root
root.render(

  <React.StrictMode>
    <App />
  </React.StrictMode>
);


/*const name = 'John Smith';
const element = <h1>Hello, {name}! </h1>

root.render(element);*/