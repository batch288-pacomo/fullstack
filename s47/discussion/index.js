// console.log("Hello!")

// [Section] Document Object Model (DOM)
    /*
allows us to acces or modify the properties of an HTML element in a webpage
it is standard on how to get, change, add or delete HTML elements
we will be focusiong only with DOM in terms of managing forms

    */

    // using the query selector it can access the HTML elements/s
        // CSS selectors to target specific element
            // id selector(#);
            // class selector(.);
            // tag/type selector(html tags);
            // universal selector();
            // attribute selector([attribute]);

    // Query selectors has two types: querySelector and querySelectorAll
// console.log(firstElement)

    // querySelector
    let secondElement = document.querySelector('.full-name');
    console.log(secondElement);
    // querySelectorAll
    let thirdElement = document.querySelectorAll('.full-name');
    console.log(thirdElement);

    // getElement
    let element = document.getElementById('fullName');
    console.log(element);

    element = document.getElementsByClassName("full-name");
    console.log(element)

// [Section] Event Listeners
    // Whenever a user interacts with a webpage, this action is considered as event
    // working with events is large part of creating interactivity in a webpage
    // specific function will be invoked if the event happen


// function 'addEventListener', it takes two arguments
    // first argument = a string identifying the event,
    // second argument = function that the listener will inboke once the specified event occur
/*    let firstElement = document.querySelector('#txt-first-name');

    firstElement.addEventListener('keydown', () => {
        console.log(firstElement.value)
        let valueInput = firstElement.value;
    })

    let txtLastName = document.querySelector('#txt-last-name');

	txtLastName.addEventListener('keyup',()=>{
		console.log(txtLastName.value);
	})
*/
   /* let fullName = document.querySelector('#fullName');
    console.log(fullName.innerHTML);
	let firstElement = document.querySelector('#txt-first-name');

    firstElement.addEventListener('change', (event) => {
        console.log(event.target.value)
        let valueInput = firstElement.value;
    })

    let txtLastName = document.querySelector('#txt-last-name');

	txtLastName.addEventListener('change',(event)=>{
		console.log(event.target.value);
	})*/
    let fullName = document.querySelector('#fullName');

	let firstElement = document.querySelector('#txt-first-name');
    firstElement.addEventListener('keyup', () => {
        fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
    })

    let txtLastName = document.querySelector('#txt-last-name');
	txtLastName.addEventListener('keyup', ()=>{
		fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
	})

	//activity starts here ////////////////////////
	let textColor = document.querySelector('#text-color');		
			textColor.addEventListener('click',()=>{
				fullName.style.color = textColor.value;
			})